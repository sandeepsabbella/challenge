openapi: "3.0.0"
info:
  version: "1.1.0"
  title: "Movie genres API"
tags:
  - name: Movie genres

paths:
  /genres/train:
    post:
      summary: "Train a model that predicts a movie's genres"
      tags:
        - Movie genres
      requestBody:
        description: |
          This endpoint receives a CSV with the columns `movie_id`, `year`, `synopsis`, and `genres`. The latter is a space-separated list of at most 5 genres that apply to the movie.
        required: true
        content:
          text/csv:
            schema:
              $ref: "#/components/schemas/Movie"
            example: |
              movie_id,year,synopsis,genres
              31711,2001,"Harry Potter is a young wizard.",Fantasy Drama Adventure Children Mystery
      responses:
        "200":
          description: "The model was trained successfully"
  /genres/predict:
    post:
      summary: "Predict genres for one or more movies"
      tags:
        - Movie genres
      requestBody:
        description: |
          This endpoint receives a CSV with the columns `movie_id`, `year` and `synopsis`. It must return a CSV with the columns `movie_id` and `predicted_genres` where the latter is a space-separated list of exactly 5 genres.
        required: true
        content:
          text/csv:
            schema:
              $ref: "#/components/schemas/MovieToPredict"
            example: |
              movie_id,year,synopsis
              13303,2001,"Frodo Baggins is a young hobbit."
      responses:
        "200":
          description: "The top 5 predicted movie genres"
          content:
            text/csv:
              schema:
                $ref: "#/components/schemas/PredictedGenres"
              example: |
                movie_id,predicted_genres
                29576,Fantasy Drama Adventure Children Mystery

components:
  schemas:
    Movie:
      required:
      - year
      - synopsis
      - genres
      properties:
        year:
          type: integer
          description: "A movie release year."
          example: 2001
        synopsis:
          type: string
          description: "A movie synopsis."
          example: "Harry Potter is a young wizard."
        genres:
          type: string
          description: "A list of at most 5 space-delimited movie genres."
          example: "Adventure Children Fantasy Mystery"
    MovieToPredict:
      required:
      - year
      - synopsis
      properties:
        year:
          type: integer
          description: "A movie release year."
          example: 2001
        synopsis:
          type: string
          description: "A movie synopsis."
          example: "Frodo Baggins is a young hobbit."
    PredictedGenres:
      required:
      - predicted_genres
      properties:
        predicted_genres:
          type: string
          description: "A list of 5 space-delimited movie genres."
          example: "Fantasy Drama Adventure Action Mystery"
